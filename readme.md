# CC 19 patch

## Work

*	Ps CC 19
*	Lr CC 19
*	Ai CC 19
*	Id CC 19
*	Ic CC 19
*	Au CC 19
*	Pr CC 19
*	Pl CC 19
*	Ch CC 19
*	Ae CC 19
*	Me CC 19
*	Br CC 19
*	An CC 19
*	Dw CC 19

## Run

### Mac

*	Via curl

	```sudo sh -c "$(curl -fsSL https://raw.githubusercontent.com/YJBeetle/cc19patch/master/cc19patch.sh)"```

*	Via wget

	```sudo sh -c "$(wget https://raw.githubusercontent.com/YJBeetle/cc19patch/master/cc19patch.sh -O -)"```

### Win

see [text.md](./text.md).

	自己动手 丰衣足食
